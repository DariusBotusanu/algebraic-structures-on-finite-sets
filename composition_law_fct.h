#include "composition_law_fct.cpp"

vector<char> read_set();
vector<vector<char>> operation_table(vector<char> A);
void print_table(vector<vector<char>> B);
int find_index(char b, vector<char> A);
bool is_commutative(vector<vector<char>> B);
bool is_associative(vector<char> A, vector<vector<char>> B);
char identity_elem(vector<char> A, vector<vector<char>> B);
bool is_invertible(char x, vector<char> A, vector<vector<char>> B);
vector<char> units(vector<char> A, vector<vector<char>> B);
bool is_semigroup(vector<char> A, vector<vector<char>> B);
bool is_monoid(vector<char> A, vector<vector<char>> B);
bool is_group(vector<char> A, vector<vector<char>> B);
string determine_algebraic_structure(vector<char> A, vector<vector<char>> B);
