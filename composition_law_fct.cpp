#include <iostream>
#include <vector>
#include <string>

using namespace std;

vector<char> read_set() 
/*
*   Reads a vector of characters from the user.  
*   The vector represents the set on which we define a composition law.
*/
{
    unsigned int cardinal;
    cout << "Enter the cardinal of your set (any non-zero natural number): ";
    cin >> cardinal;
    cout << endl;
    vector<char> A;
    for (unsigned int i = 0; i < cardinal; i++) 
    {
        char aux;
        cout << "Enter element: ";
        cin >> aux;
        A.push_back(aux);
    }
    return A;
}

vector<vector<char>> operation_table(vector<char> A)
/*
*   Returns the operation table defined by the user on the set A.
*/
{
    vector<vector<char>> operation_table;
    for (unsigned int i = 0; i < A.size(); i++)
    {
        vector<char> aux_vec;
        for (unsigned int j = 0; j < A.size(); j++)
        {
            char aux_val;
            cout << A[i] << "*" << A[j] << "= ";
            cin >> aux_val;
            aux_vec.push_back(aux_val);
        }
        operation_table.push_back(aux_vec);
    }
    return operation_table;
}


int find_index(char b, vector<char> A)
/*
*   Returns the index at which b appears in A or -1 if b does not belong to A.
*/
{
    for (unsigned int i = 0; i < A.size(); i++)
    {
        if (b == A[i])
        {
            return i;
        }
    }
    return -1;
}


bool is_commutative(vector<vector<char>> B)
/*
*   Checks if the operation defined by B on the set A is commutative.
*/
{
    for (unsigned int i = 0; i < B.size(); i++)
    {
        for (unsigned int j = 0; j < B.size(); j++)
        {
            if (B[i][j] != B[j][i])
            {
                return false;
            }
        }
    }
    return true;
}


bool is_associative(vector<char> A, vector<vector<char>> B)
/*
*   Checks if the operation defined by B on the set A is associative.
*/
{
    for (unsigned int i = 0; i < A.size(); i++)
    {
        for (unsigned int j = 0; j < A.size(); j++)
        {
            for (unsigned int k = 0; k < A.size(); k++)
            {
                int pos1 = (find_index(B[i][j], A));
                int pos2 = (find_index(B[j][k], A));
                if (B[pos1][k] != B[i][pos2])
                {
                    return false;
                }
            }
        }
    }
    return true;
}

char identity_elem(vector<char> A, vector<vector<char>> B)
/*
*   Returns the identity element of the set A w.r.t. the operation defined by B.
*   Returns ' ' if there is no identity element
*/
{
    char identity = ' ';
    for (unsigned int i = 0; i < A.size(); i++)
    {
        identity = A[i];
        for (unsigned int j = 0; j < A.size(); j++)
        {
            if (B[i][j] != B[j][i] || B[i][j] != A[j])
            {
                identity = ' ';
            }
        }
        if (identity == A[i])
        {
            return A[i];
        }
    }
    return identity;
}

bool is_invertible(char x, vector<char> A, vector<vector<char>> B)
/*
*   Checks if an element from the set A is invertible w.r.t. the operation defined by B
*/
{
    char identity = identity_elem(A, B);
    for (unsigned int i = 0; i < A.size(); i++)
    {
        if (x == A[i])
        {
            for (unsigned int j = 0; j < A.size(); j++)
            {
                if (B[i][j] == B[j][i] && B[i][j] == identity)
                {
                    return true;
                }
            }
        }
    }
    return false;
}

vector<char> units(vector<char> A, vector<vector<char>> B)
/*
*   Returns a vector of characters formed by all the invertible elements with the set A w.r.t. the operation defined by B.
*/
{
    vector<char> invertible_elems;
    for (unsigned int i = 0; i < A.size(); i++)
    {
        if (is_invertible(A[i], A, B))
        {
            invertible_elems.push_back(A[i]);
        }
    }
    return invertible_elems;
}

bool is_semigroup(vector<char> A, vector<vector<char>> B)
/*
*   Determines if the algebraic structure of the set A is a semigroup w.r.t. the operation defined by B.
*/
{
    if (is_associative(A, B))
    {
        return true;
    }
    return false;
}

bool is_monoid(vector<char> A, vector<vector<char>> B)
/*
*   Determines if the algebraic structure of the set A is a monoid w.r.t. the operation defined by B.
*/
{
    if (is_associative(A, B) && identity_elem(A, B) != ' ')
    {
        return true;
    }
    return false;
}

bool is_group(vector<char> A, vector<vector<char>> B)
/*
*   Determines if the algebraic structure of the set A is a group w.r.t. the operation defined by B.
*/
{
    if (is_monoid(A, B) && A == units(A, B))
    {
        return true;
    }
    return false;
}

string determine_algebraic_structure(vector<char> A, vector<vector<char>> B)
/*
*   Determines the algebraic structure of the set A w.r.t. the operation defined by B (assuming A is closed under the operation defined by B).
*/
{
    bool com = is_commutative(B);
    if (is_group(A, B))
    {
        if (com) {
            return "Abelian Group";
        }
        return "Group";
    }
    if (is_monoid(A, B))
    {
        if (com) {
            return "commutative Monoid";
        }
        return "Monoid";
    }
    if (is_semigroup(A, B))
    {
        if (com) {
            return "commutative Semigroup";
        }
        return "Semigroup";
    }
    if (com)
    {
        return "commutative Grupoid";
    }
    return "Grupoid";
}

void print_table(vector<vector<char>> B)
{
    unsigned int n = B.size();
    cout << string("___", n);
    for (unsigned int i = 0; i < n; i++) 
    {
        cout << string("___", n*2) << endl << "|";
        for (unsigned int j = 0; j < n; j++)
        {
            cout << B[i][j] << "|";
        } 
        cout << endl;
    }
}