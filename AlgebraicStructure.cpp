// AlgebraicStructure.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include "composition_law_fct.h"

using namespace std;



int main()
{
    vector<char> A = read_set();
    vector<vector<char>> phi = operation_table(A);
    string algebraic_struct = determine_algebraic_structure(A, phi);
    print_table(phi);
    cout << "(A, *) is a " << algebraic_struct << "." << endl;
    return 0;
}